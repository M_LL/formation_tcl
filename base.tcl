# pour exécuter le fichier, entrer dans la console tclsh:
# source "C:/myrtille/Altran/ferroviaire formation/IB formation/Tcl Tk/formation_tcl/base.tcl"

puts "C'est un vrai zoo ici!"

# la place à 8.50e, mais à 6.30e à partir de 5 personnes

set place1 8.5
set place5plus 6.3

set $a 18

# invalide:
#if { $a > 100 } { puts 100 }
#elseif { $a > 50 } { puts 50 }
#else { puts else }

# il faut } elseif sur la même ligne
#if { $a > 100 } { puts 100 
} elseif { $a > 50 } { puts 50 
} elseif { $a > 10 } { puts 10 
} else { puts else }

puts "Nombre de visiteurs :"
gets stdin visiteurs

# afficher le tarif en fonction du nombre de visiteurs
# popcorn offert si au moins 4 visiteurs, 1 paquet pour 3 visiteurs

if { $visiteurs > 4 } { puts "tarif = [expr $visiteurs * $place5plus]"
} else {puts "tarif = [expr $visiteurs * $place1]"}

if { $visiteurs > 4 }  







